from django.urls import path
from . import views

app_name = "catalogo"

urlpatterns = [
    path('', views.index, name="index"),
    path('<int:producto_id>', views.producto, name="producto"),
    path('producto_alta', views.producto_alta, name="producto_alta"),
    path('<int:producto_id>/producto_modificar', views.producto_modificar, name="producto_modificar"),
    path('<int:producto_id>/producto_eliminar', views.producto_eliminar, name="producto_eliminar"),
    path('resultado_busqueda', views.resultado_busqueda, name="resultado_busqueda"),
    path('<int:producto_id>/agregar_carrito', views.agregar_carrito, name="agregar_carrito"),
    path('<int:posicion>/quitar_carrito', views.quitar_carrito, name="quitar_carrito"),
    path('vaciar_carrito', views.vaciar_carrito, name="vaciar_carrito"),
    path('carrito', views.carrito, name="carrito"),
]