from django import forms
from django.forms import fields
from .models import Producto, Categoria

class FormProductoCustom(forms.ModelForm):
    class Meta:
        model = Producto
        fields = '__all__'
    titulo = forms.CharField(label='Título:',max_length=64,
                                    widget=forms.TextInput(
                                    attrs={
                                        'class':'form-control',
                                        'placeholder':'Nombre del producto',
                                    }), required=True)
    imagen = forms.ImageField(label='Imagen:',
                                widget=forms.FileInput(
                                attrs={
                                    'class':'form-control',
                                    'placeholder':'Foto del producto',
                                }), required=True)
    descripcion = forms.CharField(label='Descripción:',max_length=64,
                                widget=forms.Textarea(
                                attrs={
                                    'class':'form-control',
                                    'placeholder':'Descripción del producto',
                                }), required=True)
    precio = forms.DecimalField(label='Precio:',max_value=100000, decimal_places=2, min_value=0,
                                widget=forms.NumberInput(
                                attrs={
                                    'class':'form-control',
                                    'placeholder':'Precio del producto',
                                }), required=True)
    categoria = forms.ModelChoiceField(queryset= Categoria.objects.all(), empty_label="(Nada)",label='Categoría:',
                                widget=forms.Select(
                                attrs={
                                    'class':'form-control',
                                }), required=True) 
