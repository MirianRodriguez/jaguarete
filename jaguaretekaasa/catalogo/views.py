from django.forms.models import modelform_factory
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import *
from .models import Producto, Categoria
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required
#para cargar imagenes
from .models import Image
from django.shortcuts import HttpResponse
import json
import uuid
from django.db.models import Q
 
 
MEDIA_SERVER = 'http://127.0.0.1:8000/media/'
 
 

# Create your views here.

def index(request):
    
    if "carrito" not in request.session:
        request.session["carrito"] = []
    return render(request,"productos/index.html", {
        "lista_productos": Producto.objects.order_by('-id'),
        "carrito": request.session["carrito"],
    })


def producto(request, producto_id):
    producto = Producto.objects.get(id=producto_id)
    return render(request, "productos/producto.html", {
        "producto": producto,
    })

@permission_required('catalogo.add_producto')
def producto_alta(request):
    #ProductoForm = modelform_factory(Producto, fields=("titulo", "imagen", "descripcion", "precio", "categoria"))
    if request.method == "POST":
        form = FormProductoCustom(request.POST or None, request.FILES or None)
        
        

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("catalogo:index")) #nombre de la app: nombre de la ruta (urls.py)
        else:
            return render(request, "productos/producto_alta.html")
    else:
        form = FormProductoCustom()
        return render(request, "productos/producto_alta.html", {
            "formset": form
        })

@permission_required('catalogo.change_producto')
def producto_modificar(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    
    if request.method == "POST":     
        form = FormProductoCustom(request.POST, instance = un_producto) 
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse("catalogo:index"))
    else:
        form = FormProductoCustom(instance = un_producto)
        return render(request, 'productos/producto_modificar.html', {
            "un_producto": un_producto,
            "formset": form
        })

@permission_required('catalogo.delete_producto')
def producto_eliminar(request, producto_id):
    un_producto = get_object_or_404(Producto, id=producto_id)
    un_producto.delete()
    return HttpResponseRedirect(reverse("catalogo:index"))

def resultado_busqueda(request):
    search = request.GET.get('q')
    categorianombre = request.GET.get('categoriaelegida')
    if categorianombre != "":
        categoriaid = get_object_or_404(Categoria, nombre=categorianombre)
        productos = Producto.objects.filter(categoria_id = categoriaid.id)
        item = categorianombre
    else: 
        productos = Producto.objects.filter(Q(titulo__icontains=search) | Q(descripcion__icontains=search))
        item = search
    return render(request, "productos/resultado_busqueda.html",{
        "productos": productos,
        "item": item,
    })
@login_required
def carrito(request):
    carrito = []
    for producto_id in request.session["carrito"]:
        carrito.append(get_object_or_404(Producto, id=producto_id))
    return render(request, "productos/carrito.html",{
        'carrito': carrito,
    })

@login_required
def agregar_carrito(request, producto_id):
    request.session["carrito"] += [producto_id]
    return HttpResponseRedirect(reverse("catalogo:index"))

@login_required
def quitar_carrito(request, posicion):
    aux = []
    request.session["carrito"].pop(posicion-1)
    for elemento in request.session["carrito"]:
        aux.append(elemento)
    request.session["carrito"] = aux
    return HttpResponseRedirect(reverse("catalogo:carrito"))

@login_required
def vaciar_carrito(request):
    request.session["carrito"] = []
    return HttpResponseRedirect(reverse("catalogo:carrito"))



