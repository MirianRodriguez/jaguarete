from django.db import models
from django.db.models.fields.related import ForeignKey
from django.contrib.auth.models import User

# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=64)
    descripcion = models.CharField(max_length=120)

    def __str__(self):
        return f"{self.nombre} ({self.descripcion})"

class Producto(models.Model):
    titulo = models.CharField(max_length=64)
    imagen = models.ImageField (upload_to = 'fotos', blank = True)
    descripcion = models.CharField(max_length=120)
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, related_name="productos")

    def __str__(self):
        return f"{self.titulo}: {self.imagen} {self.descripcion} - {self.precio} ({self.categoria})"

class Carrito(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    productos = models.ManyToManyField(Producto, blank=True, related_name="productosDelCarrito")
    total = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return f"{self.usuario} - {self.total}"

class Image(models.Model):
         # Imagen
    img = models.ImageField(upload_to='img')
         # Tiempo de creación
    time = models.DateTimeField(auto_now_add=True)





