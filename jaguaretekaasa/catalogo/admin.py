from django.contrib import admin

# Register your models here.

from .models import Categoria, Producto, Carrito

admin.site.register(Carrito)
admin.site.register(Categoria)
admin.site.register(Producto)

