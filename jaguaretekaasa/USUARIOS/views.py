from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from .forms import *
from django.contrib.auth.models import Group


def registrarse(request):
    if request.method == 'POST':
        form = RegistroForm(request.POST)
        if form.is_valid():
            user = form.save() 
            user.groups.add(Group.objects.get(name='usuario'))         
            return HttpResponseRedirect(reverse("login"))
        else:
            form = RegistroForm()
            return render(request, 'registration/registro.html', {
                'form': form,
                'mensaje': "No se pudo crear el usuario, por favor intente de nuevo.",
            })
    else:
        if not request.user.is_authenticated:
            form = RegistroForm()
            return render(request, 'registration/registro.html', {
                'form': form
            })
        else:
            return HttpResponseRedirect(reverse("catalogo:index"))