from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class RegistroForm(UserCreationForm):
    username = forms.CharField(
        label='Usuario', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Nombre de usuario'}), max_length=150, required=True)
    password1 = forms.CharField(
        label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Requerido. Al menos 8 caracteres y no pueden ser todos números.',}), max_length=30, required=True)
    password2 = forms.CharField(
        label='Repetir Password', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Repetir contraseña', }), max_length=30, required=True)
    first_name = forms.CharField(
        label='Nombre', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Su nombre',}), max_length=30, required=False)
    last_name = forms.CharField(
        label='Apellido', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Su apellido',}), max_length=30, required=False)
    email = forms.EmailField(
        label='Email', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Un email válido',}), max_length=254, required=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )